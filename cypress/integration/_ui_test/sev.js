import { host, token, lang, sportId, marketId, includeEventGroup, eventId, windowWidth, windowHeight } from './constants'

// expect('a=').to.not.equal($Odds_Sev.text())
// cy.pause()

function mathTrunc($Value, $Decimal) {
    let Value = Number($Value.replace(',', ''))
    let Decimal = Math.pow(10, $Decimal)
    return Number(Math.trunc(Value * Decimal) / Decimal)
}

function mathTrunc2($Value, $Decimal) {
    let Value = Number($Value)
    let Decimal = Math.pow(10, $Decimal)
    return Number(Math.trunc(Value * Decimal) / Decimal)
}

function mathTrunc3($Value, $Decimal) {
    let Value = Number($Value)
    let Decimal = Math.pow(10, $Decimal)
    return Number(Math.trunc(Value * Decimal) / Decimal).toFixed($Decimal)
}

function testOddsClick($OddsType_Sev, $Odds_Sev, $Period_Sev, $BetType_Sev, $BetTypeSelection_Sev) {
    // assert bet slip is showing upon click on odds in sev
    cy.wrap($Odds_Sev).click()
    // assert message is not showing by default '您的投注单是空的，请点选赔率进行投注。'
    // cy.get('#betSession').then(($betSession) => {
    //     expect($betSession.text()).to.not.equal('您的投注单是空的，请点选赔率进行投注。')
    // })
    cy.wait(1000)
    // assert label is showing 'Place Bet'
    cy.get('.leftmenu_betsession > .leftmenu_header > .active > .clearfix > .float-left').should('contain', 'Place Bet')
    // assert label is showing 'Single'
    //cy.get('#left_panel > .leftmenu_betsession > .filter_2 > .active > div').should('contain', 'Single')
    // assert button is exists
    cy.get('#btn_placebet').should('have.id', 'btn_placebet')
    // assert textbox is focus 'Stake'
    cy.get('#betSession > .placebetWrap > .vertical_center > .placebet_input_wrap > .input_wrap > input').then(($Stake_BetSlip) => {
        expect($Stake_BetSlip).to.have.focus
    })
}

function testEventInfo_BetSlip($OddsType_Sev, $Odds_Sev, $Period_Sev, $BetType_Sev, $BetTypeSelection_Sev) {
    // assert sport image is tally between sev & bet slip
    // cy.get('#main_middle > .banner_wrapper.soccer.early_event > .event_row > .sports_icons').then(($SportImage_Sev) => {
    //     cy.get('#betSession > .bet_slip > .flex_layout > div').then(($SportImage_BetSlip) => {
    //         expect($SportImage_Sev.text()).to.equal($SportImage_BetSlip.text())
    //     })
    // })
    // assert competition is tally between sev & bet slip
    cy.get('.main_content_wrap > #main_middle > .banner_wrapper > .event_row > .league').then(($Competition_Sev) => {
        cy.get('#betSession > .bet_slip > .league').then(($Competition_BetSlip) => {
            expect($Competition_Sev.text()).to.equal($Competition_BetSlip.text())
        })
    })
    // assert team is tally between sev & bet slip
    // cy.get('#main_middle > .banner_wrapper > .score_row > .left > .team_name').then(($HomeTeam_Sev) => {
    //     cy.get('#main_middle > .banner_wrapper > .score_row > .right > .team_name').then(($AwayTeam_Sev) => {
    //         cy.get('#betSession > .bet_confirmed > .event').then(($Team_BetSlip) => {
    //             let Team_Sev = $HomeTeam_Sev.text() + ' vs ' + $AwayTeam_Sev.text()
    //             let Team_BetSlip = $Team_BetSlip.text()
    //             expect(Team_Sev).to.equal(Team_BetSlip)
    //         })
    //     })
    // })
    // assert period & bet type is tally between sev & bet slip
    cy.get('#betSession > .bet_confirmed > .vertical_center > div:nth-child(2)').then(($Period_BetType_BetSlip) => {
        let Period_BetType_Sev = $Period_Sev.text() + ' - ' + $BetType_Sev.text()
        let Period_BetType_BetSlip = $Period_BetType_BetSlip.text()
        expect(Period_BetType_Sev).to.equal(Period_BetType_BetSlip)
    })
    // assert odds type is tally between sev & bet slip
    cy.get('#betSession > .bet_confirmed > .vertical_center > .marginleft_auto').then(($OddsType_BetSlip) => {
        let OddsType_Sev
        let OddsType_BetSlip = $OddsType_BetSlip.text()
        if ($BetType_Sev.text() == 'Handicap' || $BetType_Sev.text() == 'Over / Under' || $BetType_Sev.text() == 'Odd / Even') {
            OddsType_Sev = '(' + $OddsType_Sev.text() + ')'
        }
        else {
            OddsType_Sev = '(EURO)'
        }
        expect(OddsType_Sev).to.equal(OddsType_BetSlip)
    })
    // assert bet type selection is tally between sev & bet slip
    cy.get('#betSession > .bet_slip.bet_confirmed > div:nth-child(2) > div:nth-child(1) > span').then(($BetTypeSelection_BetSlip) => {
        expect($BetTypeSelection_Sev.text()).to.equal($BetTypeSelection_BetSlip.text())
    })
    // assert odds is tally between sev & bet slip
    cy.get('#betSession > .bet_confirmed > div:nth-child(2) > .marginleft_auto > div').then(($Odds_BetSlip) => {
        expect($Odds_Sev.text().trim()).to.equal($Odds_BetSlip.text())
    })
}

function testBetReceipt($OddsType_Sev, $Odds_Sev, $Period_Sev, $BetType_Sev, $BetTypeSelection_Sev, $Stake_Input) {
    // assert sport image is tally between sev & bet slip
    // cy.get('#main_middle > .banner_wrapper.soccer.early_event > .event_row > .sports_icons').then(($SportImage_Sev) => {
    //     cy.get('#betSession > .bet_slip > .flex_layout > div').then(($SportImage_BetSlip) => {
    //         expect($SportImage_Sev.text()).to.equal($SportImage_BetSlip.text())
    //     })
    // })
    // assert competition is tally between sev & bet slip
    //
    cy.get('.main_content_wrap > #main_middle > .banner_wrapper > .event_row > .league').then(($Competition_Sev) => {
        cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .league').then(($Competition_BetSlip) => {
            expect($Competition_Sev.text()).to.equal($Competition_BetSlip.text())
        })
    })
    // assert team is tally between sev & bet slip
    // cy.get('#main_middle > .banner_wrapper > .score_row > .left > .team_name').then(($HomeTeam_Sev) => {
    //     cy.get('#main_middle > .banner_wrapper > .score_row > .right > .team_name').then(($AwayTeam_Sev) => {
    //         cy.get('#betSession > .bet_confirmed > .event').then(($Team_BetSlip) => {
    //             let Team_Sev = $HomeTeam_Sev.text() + ' vs ' + $AwayTeam_Sev.text()
    //             let Team_BetSlip = $Team_BetSlip.text()
    //             expect(Team_Sev).to.equal(Team_BetSlip)
    //         })
    //     })
    // })
    // assert period & bet type is tally between sev & bet slip
    // cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .vertical_center > div:nth-child(2)').then(($Period_BetType_BetSlip) => {
    //     let Period_BetType_Sev = $Period_Sev.text() + ' - ' + $BetType_Sev.text()
    //     let Period_BetType_BetSlip = $Period_BetType_BetSlip.text()
    //     expect(Period_BetType_Sev).to.equal(Period_BetType_BetSlip)
    // })
    // assert odds type is tally between sev & bet slip
    // cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .vertical_center > .marginleft_auto').then(($OddsType_BetSlip) => {
    //     let OddsType_Sev
    //     let OddsType_BetSlip = $OddsType_BetSlip.text()
    //     if ($BetType_Sev.text() == 'Handicap' || $BetType_Sev.text() == 'Over / Under' || $BetType_Sev.text() == 'Odd / Even') {
    //         OddsType_Sev = '(' + $OddsType_Sev.text() + ')'
    //     }
    //     else {
    //         OddsType_Sev = '(EURO)'
    //     }
    //     expect(OddsType_Sev).to.equal(OddsType_BetSlip)
    // })
    // assert bet type selection is tally between sev & bet slip
    // cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > div:nth-child(2) > div:nth-child(1) > span').then(($BetTypeSelection_BetSlip) => {
    //     expect($BetTypeSelection_Sev.text()).to.equal($BetTypeSelection_BetSlip.text())
    // })
    // assert odds is tally between sev & bet slip
    cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > div:nth-child(2) > .marginleft_auto > div').then(($Odds_BetSlip) => {
        expect($Odds_Sev.text().trim()).to.equal($Odds_BetSlip.text())
    })
    // assert stake amount is tally upon input stake
    cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .total_stake > .marginleft_auto').then(($Stake_BetSlip) => {
        expect(mathTrunc3($Stake_Input, 2)).to.equal($Stake_BetSlip.text())
    })
    // assert label is showing '注单号'
    cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .flex_wrap > .bet_ref').should('contain', '注单号')
    // assert label is showing '确认'
    cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .flex_wrap > .marginleft_auto > div:nth-child(1)').should('contain', '确认')
    // assert label is showing 'Your Bet is placed '
    cy.get('#testingonly > div:nth-child(1) > div > .text-center').should('contain', 'Your Bet is placed ')
    // assert label is showing 'Successfully'
    cy.get('#testingonly > div:nth-child(1) > div > .text-center > span').should('contain', 'Successfully')
    // assert button is showing '确认'
    cy.get('#testingonly > div:nth-child(2) > div').should('have.class', 'btn_placebet btn')
}

function testMiniBetList($OddsType_Sev, $Odds_Sev, $Period_Sev, $BetType_Sev, $BetTypeSelection_Sev, Stake_Input, WagerNo_BetSlip) {
    // assert label is showing 'My Bets'
    cy.get('#left_panel .leftmenu_header > :nth-child(2) > .clearfix > .float-left').should('contain', 'My Bets')
    cy.get('.leftmenu_betsession > .leftmenu_header > :nth-child(2)').then(($MyBets) => {
        cy.wrap($MyBets).click()
    })
    cy.wait(1000)
    // assert label is showing 'Confirmed' upon click on 'My Bets'
    cy.get('.leftmenu_betsession > .leftmenu_inner_filter > :nth-child(2)').should('contain', 'Confirmed')
    cy.get('.leftmenu_betsession > .leftmenu_inner_filter > :nth-child(2)').then(($Confirmed) => {
        cy.wrap($Confirmed).click()
        cy.wait(3000)
    })
    // assert mini bet list is showing upon click on "Confirmed"
    //cy.get('#betSession > div').each(($Wager) => {
    cy.get('#betSession > div:nth-child(2) > .flex_wrap > .bet_ref').then(($WagerNo_BetList) => {
        WagerNo_BetSlip = Number(WagerNo_BetSlip)
        let WagerNo_BetList = 0
        WagerNo_BetList = Number($WagerNo_BetList.text().replace('Ref: ', ''))
        if (WagerNo_BetSlip == WagerNo_BetList) {
            // assert sport image is tally between sev & bet slip
            // cy.get('#main_middle > .banner_wrapper.soccer.early_event > .event_row > .sports_icons').then(($SportImage_Sev) => {
            //     cy.get('#betSession > .bet_slip > .flex_layout > div').then(($SportImage_BetSlip) => {
            //         expect($SportImage_Sev.text()).to.equal($SportImage_BetSlip.text())
            //     })
            // })
            // assert competition is tally between sev & bet slip
            cy.get('.main_content_wrap > #main_middle > .banner_wrapper > .event_row > .league').then(($Competition_Sev) => {
                cy.get('#betSession > div:nth-child(2) > .league').then(($Competition_BetSlip) => {
                    expect($Competition_Sev.text()).to.equal($Competition_BetSlip.text())
                })
            })
            // //assert team is tally between sev & bet slip
            // cy.get('#main_middle > .banner_wrapper > .score_row > .left > .team_name').then(($HomeTeam_Sev) => {
            //     cy.get('#main_middle > .banner_wrapper > .score_row > .right > .team_name').then(($AwayTeam_Sev) => {
            //         cy.get('#betSession > div:nth-child(2) > .event').then(($Team_BetSlip) => {
            //             let Team_Sev = $HomeTeam_Sev.text() + ' vs ' + $AwayTeam_Sev.text()
            //             let Team_BetSlip = $Team_BetSlip.text()
            //             expect(Team_Sev).to.equal(Team_BetSlip)
            //         })
            //     })
            // })
            //assert period & bet type is tally between sev & bet slip
            // cy.get('#betSession > div:nth-child(2) > div:nth-child(1)').then(($Period_BetType_BetSlip) => {
            //     let Period_BetType_Sev = $Period_Sev.text() + ' ' + $BetType_Sev.text()
            //     let Period_BetType_BetSlip = $Period_BetType_BetSlip.text().split(' ')[0] + ' ' + $Period_BetType_BetSlip.text().split(' ')[1]
            //     expect(Period_BetType_Sev).to.equal(Period_BetType_BetSlip)

            //     // assert odds type is tally between sev & bet slip
            //     let OddsType_Sev
            //     let OddsType_BetSlip = $Period_BetType_BetSlip.text().split(' ')[3].replace('AM', '').replace('PM', '')
            //     if ($BetType_Sev.text() == 'Handicap' || $BetType_Sev.text() == 'Over / Under' || $BetType_Sev.text() == 'Odd / Even') {
            //         OddsType_Sev = '(' + $OddsType_Sev.text() + ')'
            //     }
            //     else {
            //         OddsType_Sev = '(EURO)'
            //     }
            //     expect(OddsType_Sev).to.equal(OddsType_BetSlip)
            // })
            // assert bet type selection is tally between sev & bet slip
            // cy.get('#betSession > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > .selection').then(($BetTypeSelection_BetSlip) => {
            //     expect($BetTypeSelection_Sev.text()).to.equal($BetTypeSelection_BetSlip.text().trim())
            // })
            // assert odds is tally between sev & bet slip
            cy.get('#betSession > div:nth-child(2) > div:nth-child(2) > .marginleft_auto > .odds').then(($Odds_BetSlip) => {
                expect($Odds_Sev.text().trim()).to.equal($Odds_BetSlip.text())
            })
            // assert stake amount is tally upon input stake
            cy.get('#betSession > div:nth-child(2) > .total_stake > .total_stake_amt').then(($Stake_BetSlip) => {
                expect(mathTrunc3(Stake_Input, 2)).to.equal($Stake_BetSlip.text().split(' ')[1])
            })
            // assert label is showing 'Stake: '
            cy.get('#betSession > div:nth-child(2) > .total_stake > .total_stake_title').should('contain', 'Stake: ')
            // assert label is showing 'Est. Winnings: '
            cy.get('#betSession > div:nth-child(2) > .bet_info_table > div:nth-child(1)').should('contain', 'Est. Winnings: ')
            // assert label is showing 'Ref: [WagerNo]'
            cy.get('#betSession > div:nth-child(2) > .flex_wrap > .bet_ref').should('contain', WagerNo_BetSlip)
            // assert label is showing 'Confirmed'
            cy.get('#betSession > div:nth-child(2) > .flex_wrap > .bet_status > div:nth-child(1)').should('contain', 'Confirmed')
        }
        //})
    })
}

function getStake($Stake_BetSlip) {
    let Stake_BetSlip
    const Stake = $Stake_BetSlip.map((i, el) => {
        return Cypress.$(el).attr('value')
    })
    Stake_BetSlip = Stake.get()

    if (Number(Stake_BetSlip) > 0) {
        Stake_BetSlip = mathTrunc2(Stake_BetSlip, 2)
    } else {
        Stake_BetSlip = ''
    }
    // if (Number(Stake_BetSlip) <= 0) {
    //     Stake_BetSlip = ''
    // }
    //return Number(Stake_BetSlip)
    return Stake_BetSlip
}

function getMax($e1, $e2) {
    if ($e1 > $e2) {
        return Number($e2)
    } else {
        return Number($e1)
    }
}

function testFastBetKey($Mode, $FastBetKey) {
    let Stake_BetSlip = 0
    let MaxBet_BetSlip = 0
    cy.get('#betSession > div:nth-child(3) > div:nth-child(2) > .marginleft_auto').then(($MaxBet_BetSlip) => {
        MaxBet_BetSlip = mathTrunc($MaxBet_BetSlip.text().split(' ')[1], 2)
        cy.get('.fastbet_wrap > .fastbet_item').each(($FastBetKey_BetSlip) => {
            cy.get('#betSession > .placebetWrap > .vertical_center > .placebet_input_wrap > .input_wrap > input').then(($Stake_BetSlip) => {
                let FastBetKey_BetSlip = 0
                Stake_BetSlip = getStake($Stake_BetSlip)
                if ($Mode == 0) {
                    if ($FastBetKey_BetSlip.text().toUpperCase() == 'MAX') {
                        FastBetKey_BetSlip = MaxBet_BetSlip
                    } else {
                        FastBetKey_BetSlip = getMax(mathTrunc($FastBetKey_BetSlip.text(), 2), MaxBet_BetSlip)
                    }
                } else {
                    if (mathTrunc($FastBetKey_BetSlip.text(), 2) == $FastBetKey) {
                        FastBetKey_BetSlip = getMax(mathTrunc($FastBetKey_BetSlip.text(), 2), MaxBet_BetSlip)
                    }
                }
                if (FastBetKey_BetSlip > 0) {
                    cy.wrap($FastBetKey_BetSlip).click()
                    cy.wait(1000)
                    cy.get('#betSession > .placebetWrap > .vertical_center > .placebet_input_wrap > .input_wrap > input').then(($Stake_BetSlip) => {
                        FastBetKey_BetSlip = getMax(Stake_BetSlip + Number(FastBetKey_BetSlip), MaxBet_BetSlip)
                        Stake_BetSlip = getMax(getStake($Stake_BetSlip), MaxBet_BetSlip)
                        // assert fast bet amount is accumulate to stake upon click on fast bet button
                        expect(FastBetKey_BetSlip).to.equal(Stake_BetSlip)
                        expect($Stake_BetSlip).to.have.focus
                    })
                }
            })
        })
    })
}

function testPlaceBetClick() {
    cy.get('#btn_placebet').click()
    cy.wait(3000)
}

function testConfirmClick() {
    cy.get('.btn_placebet').click()
    cy.wait(1000)
}

function testInsertStake($OddsType_Sev, $Odds_Sev, $Period_Sev, $BetType_Sev, $BetTypeSelection_Sev) {
    let Stake_Input = 0
    let Stake_BetSlip = 0
    let MaxBet_BetSlip = 0
    let WagerNo_BetSlip = 0
    // happy flow
    //cy.wrap([2, 5, 10, 100, 1000, 10000, 2.0, 2.00, 2.000, 2.001, 2.01, 2.1, 99.0, 99.00, 99.000, 99.001, 99.01, 99.1]).each(($Stake) => {
    cy.wrap([2]).each(($Stake) => {
        //testOddsClick($Odds_Sev)
        cy.get('#betSession > div:nth-child(3) > div:nth-child(2) > .marginleft_auto').then(($MaxBet_BetSlip) => {
            MaxBet_BetSlip = mathTrunc($MaxBet_BetSlip.text().split(' ')[1], 2)
            cy.get('#betSession > .placebetWrap > .vertical_center > .placebet_input_wrap > .input_wrap > input').clear().type($Stake)
            cy.get('#betSession > .placebetWrap > .vertical_center > .placebet_input_wrap > .input_wrap > input').then(($Stake_BetSlip) => {
                Stake_Input = mathTrunc2(getMax(Number($Stake), MaxBet_BetSlip), 2)
                Stake_BetSlip = getStake($Stake_BetSlip)
                // assert stake amount is tally upon input stake
                expect(Stake_Input).to.equal(Stake_BetSlip)
                expect($Stake_BetSlip).to.have.focus
                testPlaceBetClick()
                let WagerNo_BetSlip = 0
                cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .flex_wrap > .bet_ref').then(($WagerNo_BetSlip) => {
                    WagerNo_BetSlip = $WagerNo_BetSlip.text().replace('注单号: ', '')
                    testBetReceipt($OddsType_Sev, $Odds_Sev, $Period_Sev, $BetType_Sev, $BetTypeSelection_Sev, Stake_Input)
                    testConfirmClick()
                    testMiniBetList($OddsType_Sev, $Odds_Sev, $Period_Sev, $BetType_Sev, $BetTypeSelection_Sev, Stake_Input, WagerNo_BetSlip)
                })
            })
        })
    })
    // non-happy flow
    // cy.wrap(['0', '1', 0.0, 0.00, 0.000, 0.001, 0.01, 0.1, '-1', 'a', 'ab', '!', '0.']).each(($Stake) => {
    // cy.get('#betSession > div:nth-child(3) > div:nth-child(2) > .marginleft_auto').then(($MaxBet_BetSlip) => {
    //     MaxBet_BetSlip = mathTrunc($MaxBet_BetSlip.text().split(' ')[1], 2)
    //     testOddsClick($Odds_Sev)
    //     cy.wrap(['a', 'ab', '!', '0.']).each(($Stake) => {
    //         cy.get('#betSession > .placebetWrap > .vertical_center > .placebet_input_wrap > .input_wrap > input').clear().type($Stake)
    //         cy.get('#betSession > .placebetWrap > .vertical_center > .placebet_input_wrap > .input_wrap > input').then(($Stake_BetSlip) => {
    //             Stake_BetSlip = getStake($Stake_BetSlip)
    //             // assert stake amount is tally upon input stake
    //             expect('').to.equal(Stake_BetSlip)
    //             expect($Stake_BetSlip).to.have.focus
    //             testPlaceBetClick()
    //             cy.get('#betSession > .placebetWrap > .bet_slip_warning.event_info_combo > div:nth-child(2)').then(($WarningMinBet) => {
    //                 expect('Bet amount lower than min bet limit. RMB 2').to.equal($WarningMinBet.text())
    //             })
    //         })
    //     })
    // })
}

function getFastBetKey3($FastBetKey3) {
    let FastBetKey3 = 0
    const FastBetKey = $FastBetKey3.map((i, el) => {
        return Cypress.$(el).attr('value')
    })
    FastBetKey3 = mathTrunc2(FastBetKey.get(), 2)
    return FastBetKey3
}

describe('Web2.0', function () {
    let FastBetKey3
    let OddsType_Sev
    let arrPeriod_Sev
    let arrPeriod_Sev1
    let arrBetType_Sev
    let arrBetTypeSelection_Sev
    let arrOdds_Sev

    beforeEach(function () {
        cy.viewport(windowWidth, windowHeight)
        cy.visit(`${host}/popup/?tab=3&token=${token}&languageCode=${lang}`)
        cy.on('uncaught:exception', (err, runnable) => {
            return false
        })

        // cy.get(':nth-child(10) > .preferences_selections > :nth-child(3) > .normalInput').then(($FastBetKey3) => {
        //     FastBetKey3 = getFastBetKey3($FastBetKey3)
        // })
        FastBetKey3=2
    })

    beforeEach(function () {
        cy.viewport(windowWidth, windowHeight)
        cy.visit(`${host}/sev/${sportId}/${marketId}/${eventId}/${includeEventGroup}/?token=${token}&languageCode=${lang}`)
        cy.on('uncaught:exception', (err, runnable) => {
            return false
        })

        cy.get('#main_middle .main_nav_top_right .top_nav_options_text').then(($OddsType_Sev) => {
            OddsType_Sev = $OddsType_Sev
        })
    })

    // beforeEach(function () {
    //     cy.viewport(windowWidth, windowHeight)
    //     cy.visit(`${host}/sev/${sportId}/${marketId}/${eventId}/${includeEventGroup}/?token=${token}&languageCode=${lang}`)
    //     cy.on('uncaught:exception', (err, runnable) => {
    //         return false
    //     })
    //
    //     cy.get('#sev_main_wrap .bet_type_wrap > .bet_type_row > .left > .icon').then(($Period_Sev) => {
    //         arrPeriod_Sev = $Period_Sev
    //     })
    //     cy.get('#sev_main_wrap .bet_type_wrap > .bet_type_row > .left > .text').then(($BetType_Sev) => {
    //         arrBetType_Sev = $BetType_Sev
    //     })
    //     cy.get('#sev_main_wrap .bet_type_wrap .handi').then(($BetTypeSelection_Sev) => {
    //         arrBetTypeSelection_Sev = $BetTypeSelection_Sev
    //     })
    //     cy.get('#sev_main_wrap .bet_type_wrap .odds_col .odds > div').then(($Odds_Sev) => {
    //         cy.get('#sev_main_wrap .bet_type_wrap > .bet_type_row > .left > .icon').then(($Period_Sev) => {
    //             arrPeriod_Sev1 = $Period_Sev
    //         })
    //         arrOdds_Sev = $Odds_Sev
    //     })
    // })

    it('bet slip is shown upon click on odds', function () {
        cy.viewport(windowWidth, windowHeight)
        cy.visit(`${host}/sev/${sportId}/${marketId}/${eventId}/${includeEventGroup}/?token=${token}&languageCode=${lang}`)
        cy.on('uncaught:exception', (err, runnable) => {
            return false
        })
        cy.get('#tabSoccerAll').then(($All) => {
            cy.wrap($All).click()
            cy.wait(1000)
        })

        // expect('arrPeriod_Sev=').to.not.equal(arrPeriod_Sev.length)
        // expect('arrBetType_Sev=').to.not.equal(arrBetType_Sev.length)
        // expect('arrBetTypeSelection_Sev=').to.not.equal(arrBetTypeSelection_Sev.length)
        // expect('arrOdds_Sev=').to.not.equal(arrOdds_Sev.length)
        // expect('arrPeriod_Sev1=').to.not.equal(arrPeriod_Sev1.length)

        // arrPeriod_Sev.each(($Period_Sev) => {
        //     console.log('arrPeriod_Sev2=' + $Period_Sev)
        // })

        cy.get('.bet_type_wrap > .bet_type_row > .left > div:nth-child(1)').each(($Period_Sev) => {
            cy.get('.bet_type_wrap > .bet_type_row > .left > div:nth-child(2)').each(($BetType_Sev) => {
                cy.get('#sev_main_wrap .bet_type_wrap .handi').each(($BetTypeSelection_Sev) => {
                    cy.get('#sev_main_wrap .bet_type_wrap .odds > div').each(($Odds_Sev) => {
                        // action
                        testOddsClick(OddsType_Sev, $Odds_Sev, $Period_Sev, $BetType_Sev, $BetTypeSelection_Sev)
                        //testEventInfo_BetSlip($OddsType_Sev, $Odds_Sev, $Period_Sev, $BetType_Sev, $BetTypeSelection_Sev)
                        // for (var i = 0; i < 2; i++) {
                        //     testFastBetKey(1, FastBetKey3)
                        // }
                        testInsertStake(OddsType_Sev, $Odds_Sev, $Period_Sev, $BetType_Sev, $BetTypeSelection_Sev)
                        //testPlaceBetClick()
                    })
                })
            })
        })
    })
})