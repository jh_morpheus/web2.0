import {host, token, lang, sportId, marketId, includeEventGroup, evnetId, windowWidth, windowHeight } from './constants'

function mathTrunc($Value, $Decimal) {
    let Value = Number($Value.replace(',', ''))
    let Decimal = Math.pow(10, $Decimal)
    return Number(Math.trunc(Value * Decimal) / Decimal)
}

function mathTrunc2($Value, $Decimal) {
    let Value = Number($Value)
    let Decimal = Math.pow(10, $Decimal)
    return Number(Math.trunc(Value * Decimal) / Decimal)
}

function mathTrunc3($Value, $Decimal) {
    let Value = Number($Value)
    let Decimal = Math.pow(10, $Decimal)
    return Number(Math.trunc(Value * Decimal) / Decimal).toFixed($Decimal)
}

function testOddsClick($OddsType_Main, $Period, $Competition_Popular, $HomeTeamName, $AwayTeamName, $BetTypeSelection_Popular, $Odds) {
    // assert bet slip is showing upon click on odds in sev
    cy.wrap($Odds).first().click({multiple: true})
    
    cy.wait(1000)
    // assert label is showing 'Place Bet'
    cy.get('.leftmenu_betsession > .leftmenu_header > .active > .clearfix > .float-left').should('contain', 'Place Bet')
    // assert label is showing 'Single'
    cy.get('.leftmenu_inner_filter.filter_1 > .item.item_wrap.btn.active > div').should('contain', 'Single')
    // assert button is exists
    cy.get('#btn_placebet').should('have.id', 'btn_placebet')
    // assert textbox is focus 'Stake'
    cy.get('#betSession > .placebetWrap > .vertical_center > .placebet_input_wrap > .input_wrap > input').then(($Stake_BetSlip) => {
        expect($Stake_BetSlip).to.have.focus
    })
}

function testEventInfo_BetSlip($OddsType_Main, $Odds, Period, $Period, $BetType_Popular, $BetTypeSelection_Popular) {
    // assert sport image is tally between sev & bet slip
    // cy.get('#1 .icon-fi-title-s1.sports_icon').then(($SportImage_Popular) => {
    //     cy.get('#left_panel .icon-fi-title-s1.sports_icon').then(($SportImage_BetSlip) => {
    //         expect($SportImage_Popular.text()).to.equal($SportImage_BetSlip.text())
    //     })
    // })

    cy.get('#1 .icon-fi-title-s1.sports_icon').should(($className) => {
        const className = $className[0].className
        expect(className).to.match(/sports_icon-/)
        cy.get('#left_panel .icon-fi-title-s1.sports_icon').should(($className_betslip) => {
            //const classname_betslip = $div.classname_betslip
            
            let classname_betslip = $className_betslip
            //expect(classname_betslip).to.match(/-icon-fi-title-s1/)
            expect(className).to.equal(classname_betslip)
        })
    })
    // assert competition is tally between sev & bet slip
    cy.get('.mini_event_wrap > .mini_event_item_header > .competition_header_team').first().then(($Competition_Popular) => {
        cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .bet_slip.bet_confirmed > .league').then(($Competition_BetSlip) => {
            expect($Competition_Popular.text()).to.equal($Competition_BetSlip.text())
        })
    })
    // assert team is tally between sev & bet slip
    cy.get('#right_loading_overlay > .relative.main_right_inner.ps > .mini_event_wrap > .mini_event_info > .team_info_left > .flex_layout.vertical_center > div').first().then(($HomeTeam_Popular) => {
        cy.get('#right_loading_overlay > .relative.main_right_inner.ps > .mini_event_wrap > .mini_event_info > .team_info_right > .flex_layout.vertical_center > div').first().then(($AwayTeam_Popular) => {
            //cy.get('#right_loading_overlay > .relative.main_right_inner.ps > .mini_event_wrap > .mini_event_info > .event_info > .score').first().then(($TeamScore_Popular) => {
                cy.get('#betSession > .bet_confirmed > .event').then(($Team_BetSlip) => {
                    //cy.get('#betSession > .bet_confirmed > .event > .score').then(($Team_RBscore) => {           
                        let Team_Popular = $HomeTeam_Popular.text() + ' vs ' + $AwayTeam_Popular.text()
                        let Team_BetSlip = $Team_BetSlip.text()
                        //let Team_BetSlip = $Team_BetSlip.text().replace($Team_RBscore.text(), "")
                        expect(Team_Popular).to.equal(Team_BetSlip)
                    //})
                })                  
        })
    })
    // assert period & bet type is tally between sev & bet slip
    // cy.get('#betSession > .bet_confirmed > .vertical_center > div:nth-child(2)').then(($Period_BetType_BetSlip) => {
    //     //let Period_BetType = $Period.text() + ' - ' + $BetType_Popular.text()
    //     //let Period_BetType = getPeriod().text()
    //     //let Period_BetType_BetSlip = $Period_BetType_BetSlip.text()
    //     expect(getPeriod(Period)).to.equal($Period_BetType_BetSlip.text())
    // })

    // assert odds type is tally between sev & bet slip
    cy.get('#betSession > .bet_confirmed > .vertical_center > .marginleft_auto').then(($OddsType_BetSlip) => {
        cy.get('#right_panel_inner > .mini_event_wrap > .mini_eventlisting.mini_eventlisting_normal > .mini_eventlisting_header.clearfix > .event_ahou_wrap > .event_ahou_wrap_marketline .odds_title').first().then(($BetType_Popular) => {
            let OddsType_Main
            let OddsType_BetSlip = $OddsType_BetSlip.text()
            if ($BetType_Popular.text() == 'Handicap' || $BetType_Popular.text() == 'Over / Under' || $BetType_Popular.text() == 'Odd / Even') {
                OddsType_Main = '(' + $OddsType_Main.text() + ')'
            }
            else {
                OddsType_Main = '(EURO)'
            }
            expect(OddsType_Main).to.equal(OddsType_BetSlip)
        })
    })
    // assert bet type selection is tally between sev & bet slip
    // cy.get('#betSession > .bet_slip.bet_confirmed > div:nth-child(2) > div:nth-child(1) > span').then(($BetTypeSelection_BetSlip) => {
    //     expect($BetTypeSelection_Popular.text()).to.equal($BetTypeSelection_BetSlip.text())
    // })
    // assert odds is tally between sev & bet slip
    cy.get('.mini_evenlisting_content.clearfix .odds_wrap span').first().then(($Odds) => {
        cy.get('#betSession > .bet_confirmed > div:nth-child(2) > .marginleft_auto > div').then(($Odds_BetSlip) => {
            expect($Odds.text().trim()).to.equal($Odds_BetSlip.text())
        })
    })    
}

function testBetReceipt($Stake_BetSlip, $Odds, $Competition_Popular, $Period, $Odds_BetSlip, $BetType_Popular, $BetTypeSelection_Popular, Stake_Input, $Stake) {
    // assert sport image is tally between sev & bet slip
    // cy.get('#main_middle > .banner_wrapper.soccer.early_event > .event_row > .sports_ithen).then(($SportImage_Sev.first()) => {
    //     cy.get('#betSession > .bet_slip > .flex_layout > div').then(($SportImage_BetSlip) => {
    //         expect($SportImage_Sev.text()).to.equal($SportImage_BetSlip.text())
    //     })
    // })
    // assert competition is tally between sev & bet slip
    //
    // cy.get('.leftmenu_betsession_wrap > .bet_slip.bet_confirmed.flipInX.animated > .league').then(($Competition_Popular) => {
    //     cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .league').then(($Competition_BetSlip) => {
    //         expect($Competition_Popular.text()).to.equal($Competition_BetSlip.text())
    //     })
    // })
    cy.get('.mini_event_wrap > .mini_event_item_header > .competition_header_team').first().then(($Competition_Popular) => {
        cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .league').then(($Competition_BetSlip) => {
        expect($Competition_Popular.first().text()).to.equal($Competition_BetSlip.text())
        })    
    })    
    // assert team is tally between sev & bet slip
    // cy.get('#main_middle > .banner_wrapper > .score_row > .left > .team_name').then(($HomeTeam_Sev) => {
    //     cy.get('#main_middle > .banner_wrapper > .score_row > .right > .team_name').then(($AwayTeam_Sev) => {
    //         cy.get('#betSession > .bet_confirmed > .event').then(($Team_BetSlip) => {
    //             let Team_Sev = $HomeTeam_Sev.text() + ' vs ' + $AwayTeam_Sev.text()
    //             let Team_BetSlip = $Team_BetSlip.text()
    //             expect(Team_Sev).to.equal(Team_BetSlip)
    //         })
    //     })
    // })
    // assert period & bet type is tally between sev & bet slip
    // cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .vertical_center > div:nth-child(2)').then(($Period_BetType_BetSlip) => {
    //     let Period_BetType_Sev = $Period_Sev.text() + ' - ' + $BetType_Sev.text()
    //     let Period_BetType_BetSlip = $Period_BetType_BetSlip.text()
    //     expect(Period_BetType_Sev).to.equal(Period_BetType_BetSlip)
    // })
    // assert odds type is tally between sev & bet slip
    // cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .vertical_center > .marginleft_auto').then(($OddsType_BetSlip) => {
    //     let OddsType_Sev
    //     let OddsType_BetSlip = $OddsType_BetSlip.text()
    //     if ($BetType_Sev.text() == 'Handicap' || $BetType_Sev.text() == 'Over / Under' || $BetType_Sev.text() == 'Odd / Even') {
    //         OddsType_Sev = '(' + $OddsType_Sev.text() + ')'
    //     }
    //     else {
    //         OddsType_Sev = '(EURO)'
    //     }
    //     expect(OddsType_Sev).to.equal(OddsType_BetSlip)
    // })
    // assert bet type selection is tally between sev & bet slip
    // cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > div:nth-child(2) > div:nth-child(1) > span').then(($BetTypeSelection_BetSlip) => {
    //     expect($BetTypeSelection_Sev.text()).to.equal($BetTypeSelection_BetSlip.text())
    // })
    // assert odds is tally between sev & bet slip
    cy.get('.mini_evenlisting_content.clearfix .odds_wrap span').first().then(($Odds) => {
        cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .bet_slip.bet_confirmed.flipInX.animated > .flex_layout > .marginleft_auto > .odds').then(($Odds_BetSlip) => {
            // let Odds = $Odds.text()
            // let Odds_BetSlip = $Odds_BetSlip.text()
            expect($Odds.text()).to.equal($Odds_BetSlip.text())
        })
    })
    // assert stake amount is tally upon input stake
    //Stake_Input = testInsertStake(Stake_Input)
    cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .total_stake > .marginleft_auto').then(($Stake_BetSlip) => {
        expect(($Stake_BetSlip.text())).to.equal($Stake_BetSlip.text())
    })
    // assert label is showing '注单号'
    cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .flex_wrap > .bet_ref').should('contain', '注单号')
    // assert label is showing '确认'
    cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .flex_wrap > .marginleft_auto > div:nth-child(1)').should('contain', '确认')
    // assert label is showing 'Your Bet is placed '
    cy.get('#testingonly > div:nth-child(1) > div > .text-center').should('contain', 'Your Bet is placed ')
    // assert label is showing 'Successfully'
    cy.get('#testingonly > div:nth-child(1) > div > .text-center > span').should('contain', 'Successfully')
    // assert button is showing '确认'
    cy.get('#testingonly > div:nth-child(2) > div').should('have.class', 'btn_placebet btn')
}

function testMiniBetList($OddsType_Main, $Odds, $Period, $BetType_Popular, $BetTypeSelection_Popular, Stake_Input, WagerNo_BetSlip) {
    // assert label is showing 'My Bets'
    cy.get('#left_panel .leftmenu_header > :nth-child(2) > .clearfix > .float-left').should('contain', 'My Bets')
    cy.get('.leftmenu_betsession > .leftmenu_header > :nth-child(2)').then(($MyBets) => {
        cy.wrap($MyBets).click()
    })
    cy.wait(1000)
    // assert label is showing 'Confirmed' upon click on 'My Bets'
    cy.get('.leftmenu_betsession > .leftmenu_inner_filter > :nth-child(2)').should('contain', 'Confirmed')
    cy.get('.leftmenu_betsession > .leftmenu_inner_filter > :nth-child(2)').then(($Confirmed) => {
        cy.wrap($Confirmed).click()
        cy.wait(3000)
    })
    // assert mini bet list is showing upon click on "Confirmed"
    //cy.get('#betSession > div').each(($Wager) => {
    cy.get('#betSession > div:nth-child(2) > .flex_wrap > .bet_ref').then(($WagerNo_BetList) => {
        WagerNo_BetSlip = Number(WagerNo_BetSlip)
        let WagerNo_BetList = 0
        WagerNo_BetList = Number($WagerNo_BetList.text().replace('Ref: ', ''))
        if (WagerNo_BetSlip == WagerNo_BetList) {
            // assert sport image is tally between sev & bet slip
            // cy.get('#main_middle > .banner_wrapper.soccer.early_event > .event_row > .sports_icons').then(($SportImage_Sev) => {
            //     cy.get('#betSession > .bet_slip > .flex_layout > div').then(($SportImage_BetSlip) => {
            //         expect($SportImage_Sev.text()).to.equal($SportImage_BetSlip.text())
            //     })
            // })
            // assert competition is tally between sev & bet slip
            cy.get('.mini_event_wrap > .mini_event_item_header > .competition_header_team').first().then(($Competition_Popular) => {
                cy.get('#betSession > div:nth-child(2) > .league').then(($Competition_BetSlip) => {
                    expect($Competition_Popular.text()).to.equal($Competition_BetSlip.text())
                })
            })
            // //assert team is tally between sev & bet slip
            // cy.get('#main_middle > .banner_wrapper > .score_row > .left > .team_name').then(($HomeTeam_Sev) => {
            //     cy.get('#main_middle > .banner_wrapper > .score_row > .right > .team_name').then(($AwayTeam_Sev) => {
            //         cy.get('#betSession > div:nth-child(2) > .event').then(($Team_BetSlip) => {
            //             let Team_Sev = $HomeTeam_Sev.text() + ' vs ' + $AwayTeam_Sev.text()
            //             let Team_BetSlip = $Team_BetSlip.text()
            //             expect(Team_Sev).to.equal(Team_BetSlip)
            //         })
            //     })
            // })
            //assert period & bet type is tally between sev & bet slip
            // cy.get('#betSession > div:nth-child(2) > div:nth-child(1)').then(($Period_BetType_BetSlip) => {
            //     let Period_BetType_Sev = $Period_Sev.text() + ' ' + $BetType_Sev.text()
            //     let Period_BetType_BetSlip = $Period_BetType_BetSlip.text().split(' ')[0] + ' ' + $Period_BetType_BetSlip.text().split(' ')[1]
            //     expect(Period_BetType_Sev).to.equal(Period_BetType_BetSlip)

            //     // assert odds type is tally between sev & bet slip
            //     let OddsType_Sev
            //     let OddsType_BetSlip = $Period_BetType_BetSlip.text().split(' ')[3].replace('AM', '').replace('PM', '')
            //     if ($BetType_Sev.text() == 'Handicap' || $BetType_Sev.text() == 'Over / Under' || $BetType_Sev.text() == 'Odd / Even') {
            //         OddsType_Sev = '(' + $OddsType_Sev.text() + ')'
            //     }
            //     else {
            //         OddsType_Sev = '(EURO)'
            //     }
            //     expect(OddsType_Sev).to.equal(OddsType_BetSlip)
            // })
            // assert bet type selection is tally between sev & bet slip
            // cy.get('#betSession > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > .selection').then(($BetTypeSelection_BetSlip) => {
            //     expect($BetTypeSelection_Sev.text()).to.equal($BetTypeSelection_BetSlip.text().trim())
            // })
            // assert odds is tally between sev & bet slip
            cy.get('.mini_evenlisting_content.clearfix .odds_wrap span').first().then(($Odds) => {
                cy.get('#betSession > div:nth-child(2) > div:nth-child(2) > .marginleft_auto > .odds').then(($Odds_BetSlip) => {
                    expect($Odds.text().trim()).to.equal($Odds_BetSlip.text())
                })
            })    
            // assert stake amount is tally upon input stake
            cy.get('#betSession > .bet_slip.bet_confirmed > .flex_layout.total_stake > .total_stake_amt.marginleft_auto').first().then(($Stake_BetSlip) => {
                expect(mathTrunc3(Stake_Input, 2)).to.equal($Stake_BetSlip.text())
            })
            // assert label is showing 'Stake: '
            cy.get('#betSession > div:nth-child(2) > .total_stake > .total_stake_title').should('contain', 'Stake: ')
            // assert label is showing 'Est. Winnings: '
            cy.get('#betSession > div:nth-child(2) > .bet_info_table > div:nth-child(1)').should('contain', 'Est. Winnings: ')
            // assert label is showing 'Ref: [WagerNo]'
            cy.get('#betSession > div:nth-child(2) > .flex_wrap > .bet_ref').should('contain', WagerNo_BetSlip)
            // assert label is showing 'Confirmed'
            cy.get('#betSession > div:nth-child(2) > .flex_wrap > .bet_status > div:nth-child(1)').should('contain', 'Confirmed')
        }
        else {
            expect(WagerNo_BetSlip).to.equal((111))
        }
        //})
    })
}

function getMax($e1, $e2) {
    if ($e1 > $e2) {
        return Number($e2)
    } else {
        return Number($e1)
    }
}

function getStake($Stake_BetSlip){
    let Stake_BetSlip
    const Stake = $Stake_BetSlip.map((i, el) => {
        return Cypress.$(el).attr('value')
    })
    Stake_BetSlip = Stake.get()

    if (Number(Stake_BetSlip) > 0) {
        Stake_BetSlip = mathTrunc2(Stake_BetSlip, 2)
    } else {
        Stake_BetSlip = ''
    }
    // if (Number(Stake_BetSlip) <= 0) {
    //     Stake_BetSlip = ''
    // }
    //return Number(Stake_BetSlip)
    return Stake_BetSlip
}

function testPlaceBetClick() {
    cy.get('#btn_placebet').click()
    cy.wait(3000)
}

function testConfirmClick() {
    cy.get('.btn_placebet').click()
    cy.wait(1000)
}

function testInsertStake($OddsType_Main, $Period, $Stake, $BetType_Popular, $Competition_Popular, $HomeTeamName, $AwayTeamName, $BetTypeSelection_Popular, $Odds) {
    let Stake_Input = 0
    let Stake_BetSlip = 0
    let MaxBet_BetSlip = 0
    let WagerNo_BetSlip = 0
    // happy flow
    //cy.wrap([2, 5, 10, 100, 1000, 10000, 2.0, 2.00, 2.000, 2.001, 2.01, 2.1, 99.0, 99.00, 99.000, 99.001, 99.01, 99.1]).each(($Stake) => {
    cy.wrap([2]).each(($Stake) => {
        //testOddsClick($Odds_Sev)
        cy.get('#betSession > div:nth-child(3) > div:nth-child(2) > .marginleft_auto').then(($MaxBet_BetSlip) => {
            MaxBet_BetSlip = mathTrunc($MaxBet_BetSlip.text().split(' ')[1], 2)
            cy.get('#betSession > .placebetWrap > .vertical_center > .placebet_input_wrap > .input_wrap > input').clear().type($Stake)
            cy.get('#betSession > .placebetWrap > .vertical_center > .placebet_input_wrap > .input_wrap > input').then(($Stake_BetSlip) => {
                Stake_Input = mathTrunc2(getMax(Number($Stake), MaxBet_BetSlip), 2)
                Stake_BetSlip = getStake($Stake_BetSlip)
                // assert stake amount is tally upon input stake
                expect(Stake_Input).to.equal(Stake_BetSlip)
                expect($Stake_BetSlip).to.have.focus
                testPlaceBetClick()
                let WagerNo_BetSlip = 0
                cy.get('#left_panel > .leftmenu_betsession > .leftmenu_betsession_wrap > .animated > .flex_wrap > .bet_ref').then(($WagerNo_BetSlip) => {
                    WagerNo_BetSlip = $WagerNo_BetSlip.text().replace('注单号: ', '')
                    testBetReceipt($OddsType_Main, $Odds, $Competition_Popular, $HomeTeamName, $AwayTeamName, $Period, $BetType_Popular, $BetTypeSelection_Popular, Stake_Input)
                    testConfirmClick()
                    testMiniBetList($OddsType_Main, $Odds, $Competition_Popular, $BetType_Popular, $BetTypeSelection_Popular, Stake_Input, WagerNo_BetSlip)
                })
            })
        })
    })
}


function getFastBetKey3($FastBetKey3) {
    let FastBetKey3 = 0
    const FastBetKey = $FastBetKey3.map((i, el) => {
        return Cypress.$(el).attr('value')
    })
    FastBetKey3 = mathTrunc2(FastBetKey.get(), 2)
    return FastBetKey3
}

// function getPeriod(Period, $Period) {
//     cy.get('#right_panel_inner > .mini_event_wrap > .mini_event_subheader').then(($Period) =>{
//         $Period.text()
//     })
//     Period = $Period.text()
//     return Period
// }

describe('Web2.0', function () {
    let FastBetKey3
    let OddsType_Main

    // beforeEach(function (){
    //     cy.viewport(windowWidth, windowHeight)
    //     cy.visit(`${host}/popup/?tab=3&token=${token}&languagecode=${lang}`)
    //     cy.on('uncaught:exception', (err, runnable) => {
    //       return false  
    //     })

    //     cy.get(':nth-child(10) > .preferences_selections > :nth-child(3) > .normalInput').then(($FastBetKey3) => {
    //         FastBetKey3 = getFastBetKey3($FastBetKey3)
    //     })
    // })
    beforeEach(function () {
        cy.viewport(windowWidth, windowHeight)
        cy.visit(`${host}/?token=${token}&languagecode=${lang}`)
        cy.on('uncaught:exception', (err, runnable) =>{
            return false
        })

        cy.get('#main_middle > .main_nav_top.clearfix > .main_nav_top_right > .top_nav_options_grp > :nth-child(3) > .top_nav_options_text').then(($OddsType_Main)=>{
            OddsType_Main = $OddsType_Main
        })
    })

    it('bet slip is shown upon click on odds', function (){
        cy.viewport(windowWidth, windowHeight)
        cy.visit(`${host}/?token=${token}&languagecode=${lang}`)
        cy.on('uncaught:exception', (err, runnable) =>{
            return false
        })
   
        cy.get('#1 .icon-fi-title-s1.sports_icon').then(($Soccer)=> {
            cy.wrap($Soccer).click()
            cy.wait(1000)
        })

        cy.get('.mini_event_wrap').then(($Event_Popular) => {
            cy.get('.mini_event_wrap > .mini_event_item_header > .competition_header_team').then(($Competition_Popular) => {
                cy.get('.mini_event_info > .team_info_left > .flex_layout.vertical_center').then(($HomeTeamName) => {
                    cy.get('.team_info_right > .flex_layout.vertical_center').then(($AwayTeamName) => {
                    // cy.get('.event_info > .score').then(($RBScore) => {
                    //     cy.get('.event_info >. date').then(($PreTime) => {
                    //         cy.get('.event_info > .datetime').then(($DateTime) => {
                                cy.get('#right_panel_inner > .mini_event_wrap > .mini_event_subheader').then(($Period) =>{
                                    cy.get('.mini_eventlisting_header > .event_small > .odds_title').then(($BetType_Popular) => {
                                        cy.get('.event_ahou_wrap .event_ahou_wrap_marketline .event_even.clearfix .title').then(($BetTypeSelection_Popular) => {
                                            cy.get('.event_ahou_wrap .event_ahou_wrap_marketline .handi.push-right').then(($Handicap_Popular) => {
                                                cy.get('.mini_evenlisting_content.clearfix .odds_wrap span').then(($Odds) => {
                                                    //if ($Odds > 0){
                                                        testOddsClick(OddsType_Main, $Period, $Competition_Popular, $HomeTeamName, $BetType_Popular, $BetTypeSelection_Popular, $Odds)
                                                        testEventInfo_BetSlip(OddsType_Main, $Period, $Competition_Popular, $HomeTeamName, $AwayTeamName, $BetType_Popular, $BetTypeSelection_Popular, $Odds)
                                                        testInsertStake(OddsType_Main, $Period, $Competition_Popular, $HomeTeamName, $AwayTeamName, $BetType_Popular, $BetTypeSelection_Popular, $Odds)
                                                    //}
                                                })
                                            })
                                        })
                                    })
                                })   
                            //})
                        //})
                    })                                   
                })                        
            })
        })

      
               


                
            
    
    })
})